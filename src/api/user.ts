import { Router } from "express";
import { asyncWrap } from "../lib/asyncWrap";

import {
  addUser,
  getAllUsers,
  getUserById,
  getUserByName,
  deleteUserById,
  updateUser,
} from "../controllers/user";

import { uploadFakedata } from "../lib/fakeDataGeneration";

const userRoutes = Router();

userRoutes.post("/add", asyncWrap(addUser));

userRoutes.get("/", asyncWrap(getAllUsers));

userRoutes.get("/:id", asyncWrap(getUserById));

userRoutes.get("/name/:name", asyncWrap(getUserByName));

userRoutes.post("/update/:id", asyncWrap(updateUser));

userRoutes.delete("/:id", asyncWrap(deleteUserById));

userRoutes.post("/uploadfake", asyncWrap(uploadFakedata));

export default userRoutes;
