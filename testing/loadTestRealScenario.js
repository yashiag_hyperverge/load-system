import http from "k6/http";
import { Rate, Trend, Counter } from "k6/metrics";

// import * as faker from "faker/locale/en_US";
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js";
import { uuidv4 } from "https://jslib.k6.io/k6-utils/1.4.0/index.js";

const readRequestCounter = new Counter("Read Requests");
const writeRequestCounter = new Counter("Write Requests");
const readRequestTrend = new Trend("Read Request Time", true);
const writeRequestTrend = new Trend("Write Request Time", true);
const applicationTrend = new Trend("Application Run Time", true);
const applicationExistsRate = new Rate("Failed Application Checks", true);

export const options = {
  scenarios: {
    constant_request_rate: {
      executor: "constant-arrival-rate",
      rate: 10,
      timeUnit: "1s",
      duration: "20m",
      preAllocatedVUs: 10,
      maxVUs: 30,
    },
  },
};
export default function () {
  const randomName = faker.name.firstName();

  let res = http.get(`http://52.66.239.20:5007/user/name/${randomName}`);
  readRequestCounter.add(1);
  readRequestTrend.add(res.timings.duration);

  if (__VU % 2 == 0) {
    const randomName = faker.name.firstName();
    const randomDate = faker.date.between(
      "2000-01-01T00:00:00.000Z",
      "2023-01-01T00:00:00.000Z"
    );
    const randomId = uuidv4();

    const payload = JSON.stringify({
      id: randomId,
      name: randomName,
      dob: randomDate,
    });

    const params = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    let addUser = http.post(
      "http://52.66.239.20:5007/user/add",
      payload,
      params
    );

    writeRequestCounter.add(1);
    writeRequestTrend.add(addUser.timings.duration);
    applicationExistsRate.add(addUser.status !== 200);
    applicationTrend.add(addUser.timings.duration);
  }
  applicationTrend.add(res.timings.duration);
  applicationExistsRate.add(res.status !== 200);
}
