import http from "k6/http";
import { Rate, Trend } from "k6/metrics";

const applicationExistsRate = new Rate("Failed ApplicationId Checks");
const applicationExistsTrend = new Trend("ApplicationId Check Time");

export const options = {
  scenarios: {
    constant_request_rate: {
      executor: "constant-arrival-rate",
      rate: 10,
      timeUnit: "1s", // 1000 iterations per second, i.e. 1000 RPS
      duration: "1m",
      preAllocatedVUs: 3, // how large the initial pool of VUs would be
      maxVUs: 10, // if the preAllocatedVUs are not enough, we can initialize more
    },
  },
};

export default function () {
  http.get(
    "http://52.66.239.20:5007/user/9f58d5a2-fd44-44a1-93fd-2c5ff6a6b14c"
  );
}
