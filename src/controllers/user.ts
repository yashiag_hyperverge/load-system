
import { NextFunction, Request, Response } from "express";
import {
  addUserService,
  deleteUserByIdService,
  getAllUsersService,
  getUserByIdService,
  getUserByNameService,
  updateUserService,
} from "../services/UserService";

// Create a new user
export const addUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.body.id;
  const name = req.body.name;
  const dob = req.body.dob;
  console.log(userId, name, dob);
  console.log("add user");
  const resp = await addUserService(userId, name, dob);
  res.status(200).json(resp);
};

export const getAllUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const users = await getAllUsersService();
  res.status(200).json(users);
};

export const getUserById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.params.id;
  const user = await getUserByIdService(userId);
  res.status(200).json(user);
};

export const getUserByName = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userName = req.params.name;
  const users = await getUserByNameService(userName);
  res.status(200).json(users);
};

export const updateUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.params.id;
  let name = req.body.name;
  let dob = req.body.dob;

  const updatedUser = updateUserService(userId, name, dob);
  res.status(200).json(updateUser);
};

export const deleteUserById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.params.id;
  await deleteUserByIdService(userId);
  res.status(200).json({ msg: "user deleted" });
};
