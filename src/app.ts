import userRoutes from "./api/user";

const express = require("express");
const dotenv = require("dotenv");
import { Request, Response, NextFunction } from "express";
const cookieParser = require("cookie-parser");
import { sendError } from "./middlewares/error.handler";
var cors = require("cors");
const app = express();
const port: Number = parseInt(process.env.PORT || "3000");

dotenv.config();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

console.log("done");
app.use("/user", userRoutes);

//hanldes all path
app.use("*", (req: Request, res: Response, next: NextFunction) => {
  res.status(404).send("not found");
});

//common error handler
app.use(sendError);

// error handler
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.log("Error handler middleware");
  console.log("error is", err);
  res.status(500).send("server error");
});

app.listen(port, () => {
  console.log(`Listening on port: ${port}`);
});

// connectDb();

export default app;
