import { faker } from "@faker-js/faker";
import { NextFunction, Request, Response } from "express";
import { addUserDb } from "../db/user";
import { addUserService } from "../services/UserService";
import * as dotenv from "dotenv";
dotenv.config();

export const uploadFakedata = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Iteration
  // This code runs twenty times
  // It produces each time different data

  for (let i = 0; i < parseInt(process.env.ITERATIONS || "20"); i++) {
    // Initializing our variables with a different random data each time it is run
    var randomName = faker.name.firstName(); // Generates a random name
    var randomId = faker.datatype.uuid(); // It's output is a random contact card containing many properties4
    var randomDate = faker.date.between(
      "2000-01-01T00:00:00.000Z",
      "2023-01-01T00:00:00.000Z"
    );
    await addUserService(randomId, randomName, randomDate);
  }
  res.status(200).json({ msg: "done" });
};
