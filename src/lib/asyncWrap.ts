import { Request, Response, NextFunction, RequestHandler } from "express";

// export const asyncWrap = fn =>
// export const asyncWrap = (fn: any) => {
//   return function (req: Request, res: Response, next: NextFunction) {
//     // Make sure to `.catch()` any errors and pass them along to the `next()`
//     // middleware in the chain, in this case the error handler.
//     fn(req, res, next).catch(next);
//   };
// };

export const asyncWrap = (fn: Function) =>{
    return function(req: Request, res: Response, next: NextFunction) {
      Promise.resolve(fn(req, res, next)).catch(next);
    }
  }
