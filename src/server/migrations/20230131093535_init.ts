import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("user", (table: any) => {
    table.string("id").notNullable().unique();
    table.string("name").notNullable();
    table.date("dob").notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("user");
}

