import {
  addUserDb,
  deleteUserByIdDb,
  getAllUsersDb,
  getUserByIdDb,
  getUserByNameDb,
  updateUserDb,
} from "../db/user";
import { ClientError, ServerError } from "../lib/errors";

export const addUserService = async (id: string, name: string, dob: Date) => {
  try {
    const user = await addUserDb(id, name, dob);
    return user;
  } catch (err) {
    console.log(err);
    throw new ServerError("user not created");
  }
};

export const getAllUsersService = async () => {
  try {
    const user = await getAllUsersDb();
    return user;
  } catch (err) {
    console.log(err);
    throw new ServerError("user not fetchd");
  }
};

export const getUserByIdService = async (id: string) => {
  try {
    const user = await getUserByIdDb(id);
    return user;
  } catch (err) {
    throw new ServerError("user not fetchd");
  }
};

export const getUserByNameService = async (name: string) => {
  try {
    const users = await getUserByNameDb(name);
    return users;
  } catch (err) {
    throw new ServerError("user not fetchd");
  }
};

export const deleteUserByIdService = async (id: string) => {
  try {
    await deleteUserByIdDb(id);
  } catch (err) {
    throw new ServerError("user not fetchd");
  }
};

export const updateUserService = async (
  id: string,
  name: string,
  dob: Date
) => {
  try {
    const oldUser = getUserByIdDb(id);
    if (!oldUser) throw new ClientError("no such user exist");

    const user = await updateUserDb(id, name, dob);
    return user;
  } catch (err) {
    throw new ServerError("user not fetchd");
  }
};
