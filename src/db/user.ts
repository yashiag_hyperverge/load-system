import { db } from "../server/db";

export const getAllUsersDb = async () => {
  const users = await db("user");
  return users;
};

export const addUserDb = async (id: string, name: string, dob: Date) => {
  await db("user").insert({ id: id, name: name, dob: dob });
};

export const getUserByIdDb = async (id: string) => {
  const users = await db("user").where({ id: id });
  return users;
};

export const getUserByNameDb = async (name: string) => {
  const users = await db("user").where({ name: name });
  return users;
};

export const deleteUserByIdDb = async (id: string) => {
  await db("user").where("id", id).del();
};

export const updateUserDb = async (id: string, name: string, dob: Date) => {
  const user = await db("user")
    .where("id", id)
    .update({
      name,
      dob,
    })
    .returning(["id", "name", "dob"]);
  return user;
};
